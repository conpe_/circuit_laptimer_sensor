/*******************************************************************************
 * Buzz
 * 概要：
*******************************************************************************/

#include "Buzz.h"

Buzz buzz;

static uint16_t NOTE_freq[] = {
    440, 466, 494, 523, 554, 587, 622, 659, 698, 740, 784, 831, 880, 932, 988, 1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661
};


void Buzz::init(uint8_t pin=0xFF){
    _pin = pin;
}

/* 1ms */
void Buzz::task(void){
    static uint16_t play_cnt = 0;
    TONE note;
    
	if(0xFF == _pin){
		return;
	}

    if(0==play_cnt){                        /* 前音の再生時間経過 */
        if(0==popNoteBuff(&note)){     /* 次データあり */
            
            /* データセット */
            if((NOTE_NONE != note.note) && (note.note>=NOTE_A3) && (note.note<NOTE_MAX)){
                /* 音あり，音範囲OK */
                /* 指定された音階の周波数で鳴らす */
				driveTone(NOTE_freq[note.note-NOTE_A3]);
            }else{
                /* 鳴らさない */
				driveTone(0);
            }
            
            play_cnt = note.duration;                /* 再生時間設定 */
        }else{  /* 次データなし */
            /* 停める */
			driveTone(0);
        }
    }else{
        play_cnt--;
    }
}


/*
 * ブザー出力
 * 引数：
 *  BUZZ_NOTE note : 音
 *  uint16_t duration : 長さ [ms]
 * 戻値：
 *  0: 成功，1: 失敗(引数おかしい)
 * 備考：
 */
bool Buzz::sound(NOTE note, uint16_t duration){
    TONE note_tmp;
    note_tmp.note = note;
    note_tmp.duration = duration;
    
    return pushNoteBuff(&note_tmp);
}

bool Buzz::sound(TONE* notes, uint8_t data_len){
    uint16_t i;
    
    for(i=0; i<data_len; ++i){
        if(0!=pushNoteBuff(&notes[i])){
            return 1;       /* バッファ追加失敗 */
        }
    }
    
    return 0;
}

bool Buzz::soundDouble(NOTE note0, NOTE note1){
    TONE note_tmp[2];
    note_tmp[0].note = note0;
    note_tmp[0].duration = BUZZ_DURATION_DEFAULT;
    note_tmp[1].note = note1;
    note_tmp[1].duration = BUZZ_DURATION_DEFAULT;
    
    return sound(note_tmp, 2);
}

bool Buzz::pushNoteBuff(TONE* note){
    int write_p_tmp = write_p;
    
    if(write_p_tmp < TONE_BUFF_NUM-1){
        ++write_p_tmp;
    }else{
        write_p_tmp = 0;
    }

    if(write_p_tmp!=read_p){        /* 読み込みポインタに追いつかないなら書き込む */
        
        tone_buff[write_p].note = note->note;
        tone_buff[write_p].duration = note->duration;
        
        write_p = write_p_tmp;
        
        return 0;
    }else{
        return 1;
    }
}

bool Buzz::popNoteBuff(TONE* note){
    if(read_p != write_p){
        note->note = tone_buff[read_p].note;
        note->duration = tone_buff[read_p].duration;
        
        if(read_p < TONE_BUFF_NUM-1){
            ++read_p;
        }else{
            read_p = 0;
        }
        
        return 0;
    }else{
        return 1;
    }
}

bool Buzz::readNoteBuff(TONE* note){
    if(read_p != write_p){
        note->note = tone_buff[read_p].note;
        note->duration = tone_buff[read_p].duration;
        
        return 0;
    }else{
        return 1;
    }
}

#include <Arduino.h>

void Buzz::driveTone(uint16_t freq){
	ledcSetup(2, 12000, 8);
	ledcAttachPin(_pin, 2);
	ledcWriteTone(2, freq);
}

