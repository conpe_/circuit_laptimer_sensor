/*******************************************************************************

*******************************************************************************/

#ifndef CMDSRV_H
#define CMDSRV_H

#include <stdint.h>
#include <vector>
#include "CmdSrvDl.h"

#define CMDSRV_DATA_LEN_MAX         (13)		// 最大パケット長
#define CMDSRV_DF_LEN_MAX			(8)		// 最大データ長

class CmdSrv{
public:
    uint8_t notify(uint8_t* rcv_msg, uint8_t rcv_len);
    uint8_t send(uint8_t cmd_id, uint8_t* data, uint8_t len, CmdProc* cmd_proc = nullptr);    /* こちらから通信開始 */
private:
    std::vector<uint8_t> RcvBuff;
    
    std::vector<CmdProc::ST_CMD_PROC> SendCallBack;
    
    uint8_t sendPacket(uint16_t header, uint8_t cmd_id, uint8_t* data, uint8_t len, CmdProc* cmd_proc = nullptr);
    virtual uint8_t write(uint8_t* send_msg, uint8_t send_len) = 0;

    static uint8_t calcCheckSum(uint8_t* data, uint8_t len);
};

#endif //CMDSRV_H
