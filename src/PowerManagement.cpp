/*******************************************************************************
 * DataMem
 * 概要：
*******************************************************************************/
#include <Arduino.h>
#include "PowerManagement.h"

#include "BtCmdSlave.h"
#include "Buzz.h"

/* シャットオフ条件： 低電圧 */
#define MAX_BATT_mV (4100)  /* 最大電圧 */
#define LOW_BATT_mV (3400)  /* 低電圧しきい値 実測3.4V (リポは3.2Vまで使っていいらしい) */
#define BATT_V_GAIN (2.0f * 1.05f)  /* 電圧係数(抵抗での分圧分) * 実測値 */
#define LOW_BATT_SHUTDOWN_TIME (5000)  /* 低電圧検知で電源オフする時間[ms] */

/*
 * 初期化
 * 引数：
 * 戻値：
 * 備考：
 */
void PowerManagement::init(void){
    PowerState = 0;
    BattVoltage = 0;
    pinMode(PIN_KEEP_ON, OUTPUT);
    digitalWrite(PIN_KEEP_ON, 0);   /* ここではまだPWR off */
}

/*
 * 電源状態取得
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t PowerManagement::getPowerState(void){
    return PowerState;
}

/*
 * 初期化
 * 引数：
 * 戻値：
 * 備考：
 */
void PowerManagement::ctrl(void){
    static uint8_t pwr_state = 0;
    bool up_req = false;              /* 起動要求 */
    bool shutdown_req = false;        /* シャットダウン要求 */
    static uint16_t shutdown_timer = 0;
    static uint32_t low_batt_time = 0;

    uint32_t current_time = millis();
    
    
    /* バッテリ電圧取得 */
    /* 12bit = 0～4095 */
    /* max3.6V */
    uint16_t ad = analogRead(PIN_BATT);
    BattVoltage = (uint16_t)((float)ad * 3.6f / 4.095f * BATT_V_GAIN);

    if(LOW_BATT_mV <= BattVoltage){   /* 正常電圧の間，時刻をクリア */
        low_batt_time = current_time;
    }
    if(low_batt_time+LOW_BATT_SHUTDOWN_TIME < current_time){
        if(3>pwr_state){
            shutdown_timer = 0;
            pwr_state = 3;      /* シャットダウン状態へ */
        }
    }

    /* シャットオフ条件： 一度繋がって以降に切断したら，5分後にシャットダウン */
    #define DISCON_SHUTDOWN_TIME (5*60*1000)   /* 5分*60秒*1000ms */

    static bool connected = false;
    bool connected_cur = bt_cmd.isConnected();
    if(connected_cur){    /* 一度繋がったらフラグ立てる */
        connected = true;
    }
    static uint32_t discon_time = current_time;
    if(!connected || connected_cur){      /* 未接続 または 接続されている */
        discon_time = current_time;         /* 切断時刻クリア */
    }
    if(discon_time+DISCON_SHUTDOWN_TIME < current_time){
        if(3>pwr_state){
            shutdown_timer = 0;
            pwr_state = 3;      /* シャットダウン状態へ */
        }
    }

    /* シャットオフ条件： 無通信15分後にシャットダウン */
    #define DISCON_FORCE_SHUTDOWN_TIME (15*60*1000)   /* 15分*60秒*1000ms */
    static uint32_t discon_force_time = current_time;
    if(connected_cur){      /* 接続されている */
        discon_force_time = current_time;         /* 切断時刻クリア */
    }
    if(discon_force_time+DISCON_FORCE_SHUTDOWN_TIME < current_time){
        if(3>pwr_state){
            shutdown_timer = 0;
            pwr_state = 3;      /* シャットダウン状態へ */
        }
    }


    /* 長押しで起動，再度長押しでシャットダウン */
    #define BTN_POWERUP_CNT   (100)   /* 起動までのボタン長押しカウント */  
    #define BTN_SHUTDOWN_CNT  (150)   /* ボタン長押しで電源オフする時間 */
    #define SHUTDOWN_TIME     (50)    /* 終了要求から電源断までの時間(音鳴らしたりする余裕分) */

    static uint16_t btn_push_cnt = 0;
    bool pwr_sw = digitalRead(PIN_SW_PWR);  /* 正論理 */
    switch(pwr_state){
    case 0:   /* 起動中 */
        if(pwr_sw){
            if(BTN_POWERUP_CNT < btn_push_cnt){
                up_req = true;  /* 起動要求 */
                btn_push_cnt = 0;
                pwr_state = 1;
                /* 起動音 */
                buzz.sound(Buzz::NOTE_A4, 50);
                buzz.sound(Buzz::NOTE_C5, 50);
            }else{
                btn_push_cnt++;
            }
        }else{
            btn_push_cnt = 0;
        }
        break;
    case 1:   /* ボタン放し待ち */
        if(!pwr_sw){  /* ボタン離したら次の状態へ */
            pwr_state = 2;
        }
        break;
    case 2:   /* 通常動作中 */
        /* シャットダウン判定 */
        if(pwr_sw){
            if(BTN_SHUTDOWN_CNT < btn_push_cnt){
                btn_push_cnt = 0;
                pwr_state = 3;
                shutdown_timer = 0;
            }else{
                /* 押したときに鳴らす */
                if(btn_push_cnt==0){
                    buzz.sound(Buzz::NOTE_A4, 50);
                }
                btn_push_cnt++;
            }
        }else{
            btn_push_cnt = 0;
        }
        break;
    case 3: /* シャットダウン処理 */
        if(SHUTDOWN_TIME < shutdown_timer){
            pwr_state = 4;
        }else{
            if(0==shutdown_timer){
                /* シャットダウン音 */
                buzz.sound(Buzz::NOTE_C5, 50);
                buzz.sound(Buzz::NOTE_A4, 50);
            }
            shutdown_timer ++;
        }
        break;
    case 4:
        shutdown_req = true;
        break;
    }

    if(shutdown_req){ /* シャットダウン要求 */
        digitalWrite(PIN_KEEP_ON, 0);   /* PWR off */
    }else if(up_req){ /* 起動保持要求 */
        digitalWrite(PIN_KEEP_ON, 1);   /* PWR on */
    }

    PowerState = shutdown_req || (0==pwr_state) || (3<=pwr_state);
}




/*
 * コマンド 読み込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t PowerManagement::CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    float soc_tmp;

    ret_data[0] = rcv_msg[0];   /* 種別 */

    switch(rcv_msg[0]){
    case 0x00:     /* 残量 */
        *ret_len = 2;
        
        soc_tmp = (100.0f * (float)(BattVoltage - LOW_BATT_mV) / (float)(MAX_BATT_mV - LOW_BATT_mV));
        if(soc_tmp <= 100.0f){
            ret_data[1] = (uint8_t)soc_tmp;
        }else{
            ret_data[1] = 100;
        }

        break;
    case 0x01:      /* 電圧 */
        *ret_len = 3;
        ret_data[1] = BattVoltage;
        ret_data[2] = BattVoltage>>8;
        break;
    default:
        return 1;
    }
    return 0;
}

PowerManagement power_management;
