/**************************************************
	DataMemList.h
	EEPROMメモリ管理
				2020/10/13 10:01:23
**************************************************/

#ifndef __DATAMEMLIST_H__
#define __DATAMEMLIST_H__

#include "stdint.h"
#include "DataMemType.h"

/* EEPROM容量 [byte] */
#define MEM_CAPACITY 64	/* 容量(byte) */

/* EEPROM使用量 [byte] */
#define MEM_USED_BYTE 3	/* 使用量(byte) */

/* EEPROM使用ID数 */
#define MEM_ID_NUM 2	/* 使用ID数 */

/* グループ数 */
#define MEM_GROUP_NUM 1	/* グループ数 */



/* EEPROM ID */
#define	MEM_INITIALIZED	0x0000	/* EEPROM初期化済みフラグ */
#define	MEM_TIME_ADJ_PCT	0x0001	/* タイム校正値 */


/* EEPROM変数 */
extern stMem<uint8_t> mem_initialized;		/* EEPROM初期化済みフラグ */
extern stMem<uint16_t> mem_time_adj_pct;		/* タイム校正値 */


/* EEPROM管理テーブル */
extern const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM];
/* グループ管理テーブル */
extern const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM];


#endif

