/*******************************************************************************
 * SerialCmd
 * 概要：
*******************************************************************************/


#include <string.h>
#include "SerialCmd.h"

SerialCmd serial_cmd;

uint8_t SerialCmd::init(void){
    serial.begin(115200);
    serial.flush();
	
	return 0;
}

void SerialCmd::task(void){

	//connected = SerialBT.connected();
	connected = true;

	if(!connected){

	}else{	/* 接続OK */
		
		/* 受けたコマンド処理 */
		uint8_t rcv_msg[10];
		uint8_t rcv_len = 0;
		
		while(serial.available() && (rcv_len<10)){
			rcv_msg[rcv_len] = serial.read();
			++rcv_len;
		}

		notify(rcv_msg, rcv_len);

	}

}


uint8_t SerialCmd::write(uint8_t* send_msg, uint8_t send_len){
	serial.write(send_msg, send_len);

	return 0;
}


