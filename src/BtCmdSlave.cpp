/*******************************************************************************
 * BtCmdSlave
 * 概要：
*******************************************************************************/
/* 参考：https://github.com/espressif/arduino-esp32/tree/1977370e6fc069e93ffd8818798fbfda27ae7d99/libraries/BluetoothSerial */


#include <string.h>
#include "BtCmdSlave.h"


/*  */
BtCmd bt_cmd;


uint8_t BtCmd::my_mac[6];
char BtCmd::my_name[20];

uint8_t BtCmd::init(void){
	static esp_spp_cb_t cb = &BtCmd::esp_spp_cb;
	esp_read_mac(my_mac, ESP_MAC_BT);	/* 自身のMACアドレスを取得 */
	/* BT検索して出る名前を，LapSens_XXXXとする (XXXXはMACアドレス後方2byte) */
	sprintf(my_name, "LapSens_%02X%02X", my_mac[4], my_mac[5]);
	SerialBT.begin(my_name, false);		/* Slave側として開始 */
	SerialBT.register_callback(&cb);
	
	return 0;
}

void BtCmd::task(void){
	static uint16_t cnt = 0;

	connected = SerialBT.connected();

	switch(connected){
	case 0:	/* 接続待ち */
		cnt++;
		if(cnt>300){
			//SerialBT.connect(sens_address);		/* 接続しに行く */
			cnt = 0;
		}
		break;
	case 1:	/* 接続OK */
		
		/* 受けたコマンド処理 */
		uint8_t rcv_msg[10];
		uint8_t rcv_len = 0;
		
		while(SerialBT.available() && (rcv_len<10)){
			rcv_msg[rcv_len] = SerialBT.read();
			++rcv_len;
			
			//buzz.noteSingle(BUZZ_NOTE::BUZZ_NOTE_C4);
			//M5.Speaker.tone(1200, 50);
		}

		notify(rcv_msg, rcv_len);

		break;
	}

}


uint8_t BtCmd::write(uint8_t* send_msg, uint8_t send_len){
	if(!connected){
		return 1;
	}
	
	SerialBT.write(send_msg, send_len);

	return 0;
}

void BtCmd::esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){

	switch((uint8_t)event){
	case ESP_SPP_OPEN_EVT:		/* Client connection open */
	//	connected = 1;
		break;
	case ESP_SPP_CLOSE_EVT:		/* Client connection closed */
	//	connected = 0;
		break;
	default:
		break;
	}

	Serial.printf("bt_event %d\r\n", (uint8_t)event);
	
	if(28 == event){
		Serial.printf("bt_name : %s\r\n", my_name);
		Serial.printf("bt_adrs : ");
		for(int i=0; i<6; ++i){
			if(5>i){
				Serial.printf("%02X:", (uint8_t)my_mac[i]);
			}else{
				Serial.printf("%02X\r\n", (uint8_t)my_mac[i]);
			}
		}
	}
}

