/**************************************************
	DataMemList.cpp
	EEPROMメモリ管理
				2020/10/13 10:01:23
**************************************************/

#include "DataMemList.h"


/* EEPROM変数 */
stMem<uint8_t> mem_initialized = {1, 0, 1};		/* EEPROM初期化済みフラグ */
stMem<uint16_t> mem_time_adj_pct = {10000, 9000, 11000};		/* タイム校正値 */


/* EEPROM管理テーブル */
const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN],		DataId,		Adrs,		DataType,		InitVal,		MinVal,		MaxVal,		ExternalAccessPermission,		*/
	{	"", MEM_INITIALIZED,		0x0000,		MEM_UINT8,		&mem_initialized.Init,		&mem_initialized.Min,		&mem_initialized.Max,		DATAMEM_PERMISSION_READ_ONLY,		},		/* EEPROM初期化済みフラグ (Resolution=1, Offset=0) */
	{	"time_adj_pct", MEM_TIME_ADJ_PCT,		0x0001,		MEM_UINT16,		&mem_time_adj_pct.Init,		&mem_time_adj_pct.Min,		&mem_time_adj_pct.Max,		DATAMEM_PERMISSION_FULL,		},		/* タイム校正値 (Resolution=0.0001, Offset=3.3267) */
};

/* ・Name無しは表示しない */
/* ・DataIdの上位1byteがグループID */

/* グループ管理テーブル */
const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN]	*/
	{	"Util"	},		/* Group0 Util */
};
