/*******************************************************************************

*******************************************************************************/

#ifndef DRVMEM_H
#define DRVMEM_H

#include <stdint.h>

class DrvMem{
public:
    DrvMem(void){};
    virtual ~DrvMem(void){};
    
    virtual int begin(void) = 0;
    virtual int close(void) = 0;
    virtual int read(uint16_t adrs, uint8_t* data, uint8_t len) = 0;
    virtual int write(uint16_t adrs, uint8_t* data, uint8_t len) = 0;
};


#endif //DRVMEM_H
