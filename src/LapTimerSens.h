/*******************************************************************************

*******************************************************************************/

#ifndef LAPTIMERSENS_H
#define LAPTIMERSENS_H

#include <stdint.h>


class LapTimerSens{
public:
    static void task(void);
    static void detect(void);
private:
    static bool Detect;
    static uint32_t DetectTime;
    static uint32_t StartTime;
    static uint32_t LastTime;
};


extern LapTimerSens lap_timer_sens;

#endif //LAPTIMERSENS_H
