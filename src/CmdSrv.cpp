/*******************************************************************************
 * DataMem
 * 概要：
*******************************************************************************/

#include <string.h>

#include "CmdSrv.h"
#include "CmdSrvDl.h"

#define CMDSRV_RCV_LEN      (32)        /* 最大受信データ数 */

#define CMDSRV_HEADER_RCV 			(0xFBBF)	// ヘッダ
#define CMDSRV_HEADER_RET_OK 		(0xBFFB)	// ACK
#define CMDSRV_HEADER_RET_NG 		(0xCFFC)	// NACK
#define CMDSRV_DATA_LEN_WO_DF		(5)			// DataFieldを除いたパケット長
#define CMDSRV_DATA_LEN_WO_DF_SUM	(4)			// DataFieldとチェックサムを除いたパケット長

// パケット構造
#define E_UP_HEAD_IDX 0
#define E_LO_HEAD_IDX 1
#define E_LO_CMD_IDX 2
#define E_DLC_IDX 3
#define E_DATA_IDX 4


/*
 * コマンド受信処理
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdSrv::notify(uint8_t* rcv_msg, uint8_t rcv_len){
    
    /* コマンドを解析 */

#if 0
	static Clock clk = Clock();
	/* 受信タイムアウト */
	if(E_UP_HEAD_IDX < BufDataIdx){			/* データ受信始まってる */
		if(100 < clk.now()){				/* データ受信始まってから100ms以上経ってる */
			BufDataIdx = E_UP_HEAD_IDX;		/* はじめから */
		}
	}else{									/* ヘッダ受信ならリセット */
		clk.reset();
	}
#endif

	/* 受信データをバッファに貯める */
	for(int i=0; rcv_len>i; ++i){
		RcvBuff.push_back(rcv_msg[i]);
	}

	/* 最小データ数に満たない */
	while(CMDSRV_DATA_LEN_WO_DF <= RcvBuff.size()){

		/* ヘッダの位置を探す */
		uint8_t header_pos = 0;
		for(header_pos = 0; RcvBuff.size() > header_pos; ++header_pos) {
			
			if((header_pos+1) >= RcvBuff.size()){	/* ヘッダ分の受信数足りない */
				break;
			}

			uint16_t header_tmp = ((uint16_t)RcvBuff[header_pos]<<8) | (uint16_t)RcvBuff[header_pos+1];

			if( (header_tmp == CMDSRV_HEADER_RCV)
				|| (header_tmp == CMDSRV_HEADER_RET_OK)
				|| (header_tmp == CMDSRV_HEADER_RET_NG)
			){
				/* ヘッダ発見 */
				break;
			}
		}

		/* ヘッダ見つからず */
		if((header_pos+1) >= RcvBuff.size()){
			/* 最後の1byteはヘッダの先頭かもしれないので */
			/* そこだけ残して消す */
			RcvBuff.erase(RcvBuff.begin(), RcvBuff.end());	/* beginからendの手前まで消える */
			return 1;
		}

		/* ヘッダ部を先頭にする */
		RcvBuff.erase(RcvBuff.begin(), RcvBuff.begin()+header_pos);

		/* 最小データ数に足りず */
		if(CMDSRV_DATA_LEN_WO_DF > RcvBuff.size()){
			return 1;
		}

		uint8_t dlc = RcvBuff[E_DLC_IDX];
		/* データ数に足りず */
		if((CMDSRV_DATA_LEN_WO_DF+dlc) > RcvBuff.size()){
			return 1;
		}


		/* チェックサム確認 */
		uint8_t RcvSum = RcvBuff[dlc + CMDSRV_DATA_LEN_WO_DF_SUM];
		uint8_t CalcSum = calcCheckSum(&RcvBuff[header_pos], dlc + CMDSRV_DATA_LEN_WO_DF_SUM);

		if(RcvSum == CalcSum){		/* チェックサム一致 */
			/* コールバック実行 */
			uint8_t cmd_id = RcvBuff[E_LO_CMD_IDX];
			
			if(RcvBuff[E_UP_HEAD_IDX] == (CMDSRV_HEADER_RCV>>8)){
				/* コマンド受信時の応答処理 */

				/* デフォルト応答データ */
				uint8_t cmd_proc_ret = 1;       /* NG応答 */
				uint8_t retdata_len = dlc;
				uint8_t RetDataTmp[CMDSRV_DATA_LEN_MAX];
				
				/* コマンドを実行 */
				for(uint8_t src_tbl=0; CmdProc::attached_cmd_num>src_tbl; ++src_tbl){
					if(cmd_id == CmdProc::stCmdProcTbl[src_tbl].CmdId){    /* コマンドID一致 */
						cmd_proc_ret = CmdProc::stCmdProcTbl[src_tbl].cmdProc->CallBack(&RcvBuff[E_DATA_IDX], dlc, RetDataTmp, &retdata_len);
						break;
					}
				}

				/* 応答を返す */
				uint16_t header;
				if(0==cmd_proc_ret){	/* OK */
					header = CMDSRV_HEADER_RET_OK;
				}else{					/* NG */
					header = CMDSRV_HEADER_RET_NG;
				}

				sendPacket(header, cmd_id, RetDataTmp, retdata_len);

			}else if(RcvBuff[E_UP_HEAD_IDX] == (CMDSRV_HEADER_RET_OK>>8)){
				/* コマンド応答受信時の処理 */

				for(auto itr = SendCallBack.begin(); itr!=SendCallBack.end();){
					/* コマンドIDが一致するコールバックを実行して登録削除 */
					if(cmd_id == itr->CmdId){
						/* 一致 */
						itr->cmdProc->CallBack(&RcvBuff[E_DATA_IDX], dlc, nullptr, nullptr);	/* コールバック実行 */
						itr = SendCallBack.erase(itr);										/* 登録削除 */
						break;
					}else{
						++itr;	/* 一致しなければ次を確認 */
					}
				}

			}

			/* 今回処理分まで削除 */
			RcvBuff.erase(RcvBuff.begin(), RcvBuff.begin() + dlc + CMDSRV_DATA_LEN_WO_DF);

		}else{
			/* チェックサム不一致 */
			/* データの途中に正しいヘッダがあるかもしれないので今回処理したヘッダ部だけ消す */
			RcvBuff.erase(RcvBuff.begin());

			return 2;
		}
	}

	return 0;
}



uint8_t CmdSrv::send(uint8_t cmd_id, uint8_t* data, uint8_t len, CmdProc* cmd_proc){
	return sendPacket(CMDSRV_HEADER_RCV, cmd_id, data, len, cmd_proc);       /* 送信 */
}

uint8_t CmdSrv::sendPacket(uint16_t header, uint8_t cmd_id, uint8_t* data, uint8_t len, CmdProc* cmd_proc){
	uint8_t send_data[CMDSRV_DATA_LEN_MAX];

	send_data[E_UP_HEAD_IDX] = header >> 8;
	send_data[E_LO_HEAD_IDX] = header & 0x00FF;
	
	send_data[E_LO_CMD_IDX] = cmd_id;
	send_data[E_DLC_IDX] = len;

	memcpy(&send_data[E_DATA_IDX], data, len);
	
	send_data[E_DATA_IDX + len] = calcCheckSum(send_data, CMDSRV_DATA_LEN_WO_DF_SUM + len);	/* サム値をセット */
	
	uint8_t ret = write(send_data, CMDSRV_DATA_LEN_WO_DF + len);

	if(0 == ret){       /* 送信成功 */
		if(nullptr != cmd_proc){	/* コールバック登録あり */
			SendCallBack.push_back(CmdProc::ST_CMD_PROC(cmd_id, cmd_proc));
		}
		return 0;
	}else{
		return 1;
	}
}


/*
 * チェックサム計算
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdSrv::calcCheckSum(uint8_t* data, uint8_t len){
    uint8_t i;
	uint8_t sum = 0;
	
    for(i = 0; i < len; i++){
		sum += data[i];
	}

	return sum;
}