/*******************************************************************************
 * LapTimerSens
 * 概要：
*******************************************************************************/

#include "LapTimerSens.h"
#include "BtCmdSlave.h"
#include "SerialCmd.h"
#include "DataMem.h"
#include "Buzz.h"

LapTimerSens lap_timer_sens;

#define LAP_DETECT_MIN_INTERVAL_TIME  1000  /* 最低ラップタイム ms */

bool LapTimerSens::Detect = false;
uint32_t LapTimerSens::DetectTime = 0;
uint32_t LapTimerSens::StartTime = 0;
uint32_t LapTimerSens::LastTime = 0;

/*
 * 周期タスク
 * 引数：
 * 戻値：
 * 備考：
 */
void LapTimerSens::task(void){

	/* 検知 */
	if(Detect && (LastTime + LAP_DETECT_MIN_INTERVAL_TIME < DetectTime)){
		uint32_t lap_detect_time;
		
		buzz.sound(Buzz::NOTE_C4, 50);

		/* 初回検知 */
		/* 開始時刻を記録 */
		if(0 == StartTime){
			StartTime = DetectTime;
		}
		
		lap_detect_time = DetectTime - StartTime;

		/* 時刻補正 */
		uint16_t hosei;
		float adjst_pct = 100.0f;
		if(!DataMem::read(MEM_TIME_ADJ_PCT, &hosei)){	/* 読み込み */
			/* 読み込みOK */
			adjst_pct = (float)hosei/100.0f; 
		}

		lap_detect_time *= (adjst_pct/100.0f);


		/* BT繋がってる */
		if(bt_cmd.isConnected()){
			// 送信
			bt_cmd.send(0x10, (uint8_t*)&lap_detect_time, 4);
		}
		/* シリアル繋がってる */
		if(serial_cmd.isConnected()){
			// 送信
			serial_cmd.send(0x10, (uint8_t*)&lap_detect_time, 4);
		}

		LastTime = DetectTime;
	}


}


/*
 * 検知
 * 引数：
 * 戻値：
 * 備考：
 */
void LapTimerSens::detect(void){
    Detect = true;
	DetectTime = millis();
}

