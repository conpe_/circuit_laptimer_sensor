# circuit_laptimer_sensor

サーキットの各セクションに埋まっている磁石を検知してラップタイムを計ります。  
このリポジトリはセンサ側です。磁気(の変化)を検知して、前回変化時からの時間を親機へ送信します。

親機はこれら。M5StackだったりAndroid端末だったり。
* https://gitlab.com/conpe_/laptimerc
* https://gitlab.com/conpe_/circuit_laptimer_disp

## 使用方法
### 充電
USB接続によりバッテリーを充電します。
赤LEDが消灯すると充電完了。\
バッテリー持続時間は1時間強。

### 電源
ボタン長押しで電源ON。再度長押しでOFF。

### 接続
電源を入れるとステータスLED(緑)が点滅し、
親機との接続が完了すると点灯に変わります。

## 通信仕様
BluetoothSPPで、検知ごとに時間を送る。  
電源投入後最初の検知を0msとし、検知した時刻をms単位で送信。

## ソフト
### 開発環境
ESP32をArduinoで。
* arduino-esp32 Ver.2.06  
https://github.com/espressif/arduino-esp32

### ビルド設定
* CPU Frequency : 160MHz (WiFi/BT)
* Flash Frequency : 40MHz

バッテリー持ちを考慮して低めの周波数を推奨。  
CPU側を80MHzにすると起動音が鳴らない不具合あり。

### 書き込み方法
IO0スイッチを押したまま電源スイッチを押す。  
電源スイッチを押したまま、書き込みを実行する。  
書き込みが完了したら電源スイッチを放す(IO0はすぐに放してOK)。

ENは押さなくてOK。

### 注意点
* ソフト書き込み後の初回起動時は、spiffsの初期化に数十秒かかる。  
起動する(ピッと鳴る)まで、電源ボタンを押しっぱなしにすること。  
* FDTIのFT234のドライバ

## 基板
doc/schフォルダに回路図と実体配線図。  
タカチのWC72-Nのケースに合うサイズです。

### 開発環境
* EAGLE 5.11.0

### 基板エラッタ・改良案
* FT234XDのRESET接続忘れ
* FT234XDのVCCIO供給が電源スイッチ経由なので面倒。電源ボタン押しながらUSB接続しないとソフト書き込み不可。\
暫定対応として、VCC2からの供給をパターンカット。3V3OUTをVCCIOとRESETに接続。3V3OUTにはキャパシタ要。
* USB端子にキャップ取り付けられるよう、端子の位置を調整したい。
* 磁気センサのパッドが無駄に長い